# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import spacy
import os
import re
import pandas as pd
from gensim.models import LdaMulticore
from gensim.models.coherencemodel import CoherenceModel
from gensim.corpora import Dictionary
from string import punctuation
from nltk.corpus import stopwords
from tqdm import tqdm
import pickle
import argparse
from langdetect import detect
import multiprocessing
tqdm.pandas()

nlp = spacy.load("en_core_web_sm")


def remove_words(list_of_tokens, list_of_words):
    return [token for token in list_of_tokens if token not in list_of_words]


def apply_stemming(list_of_tokens, stemmer):
    return [stemmer.stem(token) for token in list_of_tokens]


def two_letters(list_of_tokens):
    # removes any words composed of less than 2 or more than 21 letters
    two_letter_word = []
    for token in list_of_tokens:
        if len(token) <= 2 or len(token) >= 21:
            two_letter_word.append(token)
    return two_letter_word


STOPWORDS = stopwords.words('english')
PUNCTUATION = list(punctuation)


def preprocess(text):
    text = ' '.join(text)
    # Removes line breaks
    text = text.rstrip('\n')
    # Makes all letters lowercase
    text = text.casefold()
    # removes specials characters and leaves only words
    text = re.sub(r'\W_', ' ', text)
    # removes numbers and words concatenated with numbers IE h4ck3r.
    # Removes road names such as BR-381.
    text = re.sub(r"\S*\d\S*", " ", text)
    # removes emails and mentions (words with @)
    text = re.sub(r"\S*@\S*\s?", " ", text)
    # removes URLs with http
    text = re.sub(r'http\S+', '', text)
    # removes URLs with www
    text = re.sub(r'www\S+', '', text)
    text = text.replace('_', '')

    if detect(text) == 'en':

        list_of_tokens = nlp(text)
        # Take only the nouns
        list_of_tokens = [
            token.text for token in list_of_tokens if 'NN' in token.tag_]
        two_letter_word = two_letters(list_of_tokens)

        list_of_tokens = remove_words(list_of_tokens, PUNCTUATION)
        list_of_tokens = remove_words(list_of_tokens, STOPWORDS)
        list_of_tokens = remove_words(list_of_tokens, two_letter_word)

        return list_of_tokens
    return []


def get_lda_topics(model, num_topics):
    word_dict = {}
    for i in range(num_topics):
        words = model.show_topic(i, topn=30)
        word_dict['Topic # ' +
                  '{:02d}'.format(i + 1)] = [i[0] for i in words]
    return pd.DataFrame(word_dict)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--file', type=str, default="../DATA/twitter-trend/MayJunJul_analysis_new.pickle",
                        help='Train file (*.pickle)')
    parser.add_argument('--num_topics', type=int, default=12,
                        help='Number of topics')

    args = parser.parse_args()
    file = args.file
    num_topics = int(args.num_topics)

    # Loading the data just to see it .head()
    with open(file, 'rb') as f:
        data = pickle.load(f)

    print(data.head())

    if not os.path.exists('corpus'):
        #
        #        MAX = 5000
        #        data = data.iloc[0:MAX]

        print('Initial:', len(data))

        print('Preprocessing')
        data['token'] = data['token'].progress_apply(lambda x: preprocess(x))

        data['len'] = data['token'].progress_apply(lambda x: len(x))

        data = data[data.len > 0]
        print('After removal of other languages:', len(data))

        data = data[data.len > 5]
        print('After removal of short texts:', len(data))

        data["date"] = pd.to_datetime(data["date"])
        data = data.sort_values(by="date")
        time_counts = data['date'].value_counts()

        # time_sequences = [3, 7]  # first 3 documents are from time slice one
        # This is for the dynamic topic modeling (not used for now)
        time_counts = time_counts.sort_index()
        time_sequences = list(time_counts.values)

        print(data.head())

        texts = data.token.tolist()

        # The preprocessing takes too long, saving the model
        print('Saving corpus')
        with open('corpus', 'wb') as f:
            pickle.dump(texts, f)
    else:
        print('Loading corpus')
        with open('corpus', 'rb') as f:
            texts = pickle.load(f)

    # Create a dictionary representation of the documents
    dictionary = Dictionary(texts)
    print('Number of unique words in initial documents:', len(dictionary))

    # Filter out words that occur less than 10 documents, or more than 20% of
    # the documents
    dictionary.filter_extremes(no_below=10, no_above=0.2)
    print(
        'Number of unique words after removing rare and common words:',
        len(dictionary))

    # Data bag-of-words
    corpus = [dictionary.doc2bow(doc) for doc in texts]

    coherences = []
    for k in range(5, num_topics):
        # Build LDA model
        print('Extracting', k, 'topics')
        lda_model = LdaMulticore(
            workers=multiprocessing.cpu_count(),
            corpus=corpus,
            id2word=dictionary,
            num_topics=k,
            random_state=100,
            chunksize=500,
            passes=20,
            per_word_topics=True)

        cm = CoherenceModel(
            model=lda_model, texts=texts,
            dictionary=dictionary, coherence='c_v')

        print('Saving model to model_lda_{}.gensim'.format(k))
        lda_model.save('model_lda_{}.gensim'.format(k))

        coherences.append((k, cm.get_coherence()))
        print('Coherence:', cm.get_coherence())

        lda_topics = get_lda_topics(lda_model, k)
        print(lda_topics)

        print('Saving topics to topics_lda_{}.csv'.format(k))
        lda_topics.transpose().to_csv('topics_lda_{}.csv'.format(k))

        topics = lda_model.print_topics(num_words=10)

    print('Coherences:', coherences)

    fig = plt.figure()
    plt.plot([coherence[0] for coherence in coherences], [coherence[1]
                                                          for coherence in coherences])
    plt.scatter([coherence[0] for coherence in coherences],
                [coherence[1] for coherence in coherences])
    plt.title('Number of Topics vs. Coherence')
    plt.xlabel('Number of Topics')
    plt.ylabel('Coherence')
    plt.xticks([coherence[0] for coherence in coherences])
    plt.savefig("plot_coherences.png", bbox_inches='tight')
